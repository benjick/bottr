import { html } from 'snabbdom-jsx';
import { goto } from '../actions/router';

const MenuItem = ({ current, title, href }) =>
  <a
    class-header-tab={true}
    class-is-active={href===current}
    on-click={e =>
      goto(e, href)
    }
    href={href}>
    {title}
  </a>;

export default MenuItem;
