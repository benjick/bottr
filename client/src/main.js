import { html } from 'snabbdom-jsx';
import { patch } from './lib/snabbdom';
import { setUrl } from './actions/router';
import App from './App';

import store from './lib/redux';

let vnode = document.getElementById('root');

function updateUI(state) {
  const newVnode = <App state={state} route={state.url} />;
  vnode = patch(vnode, newVnode);
}

store.subscribe(() =>
  updateUI(store.getState())
)

updateUI(store.getState());

window.addEventListener('popstate', () => {
  setUrl(window.location.pathname)
})
