import { html } from 'snabbdom-jsx';

import Header from './containers/Header';

import Home from './containers/Home';
import Creator from './containers/Creator';

import store from './lib/redux';
import { test } from './actions/test';

const Router = ({route}) => {
  switch (route) {
  case '/editor':
    return <Creator />
    break;
  default:
    return <Home />
  }
}

const App = ({state, route}) =>
  <div>
    <Header route={route} />
    <Router route={route} />
  </div>

export default App;
