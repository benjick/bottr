import store from '../lib/redux';

function pushState(url) {
  if (url !== window.location.pathname) {
    window.history.pushState({}, '', url);
  }
}

export function setUrl(text) {
  pushState(text);
  store.dispatch({
    type: 'SET_URL',
    text
  });
}

export function goto(event, url) {
  event.preventDefault();
  setUrl(url)
}
