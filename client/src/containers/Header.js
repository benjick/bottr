import { html } from 'snabbdom-jsx';

import MenuItem from '../components/MenuItem';

const Header = ({route}) =>
  <header className="header">
    <div className="container">
      <div className="header-left">
        <a className="header-item" href="#">
          <img src="images/preview.png" alt="Logo" />
        </a>
        <MenuItem title="Home" href="/" current={route} />
        <MenuItem title="Editor" href="/editor" current={route} />
      </div>

      <div className="header-right header-menu">
        <span className="header-item">
          <a className="button" href="/api/login">Login</a>
        </span>
      </div>
    </div>
  </header>;

export default Header;
