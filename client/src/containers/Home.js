import { html } from 'snabbdom-jsx';

const Home = () =>
  <section className="hero">
    <div className="hero-content">
      <div className="container">
        <h1 className="title">
          Welcome to Bottr!
        </h1>
        <h2 className="subtitle">
          Scripting like cray since 1999
        </h2>
      </div>
    </div>
  </section>;

export default Home;
