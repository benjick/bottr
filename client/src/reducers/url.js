export default function counter(state = window.location.pathname, action) {
  switch (action.type) {
  case 'SET_URL':
    return action.text
  default:
    return state
  }
}
