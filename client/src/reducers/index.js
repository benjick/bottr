import { combineReducers } from 'redux'
import counter from './counter'
import url from './url'

export default combineReducers({
  counter,
  url,
})
