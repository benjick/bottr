import BrowserSync from 'browser-sync';
import build from './build';
import historyApiFallback from 'connect-history-api-fallback';

const sync = BrowserSync.create();

function serve() {
  sync.init({
    server: './static',
    port: 3001,
    middleware: [ historyApiFallback() ],
  });

  sync.watch('./static/*.html').on('change', sync.reload);
  sync.watch('./static/*.css').on('change', sync.reload);

  sync.watch('./src/**/*.js').on('change', () => {
    build('development')
    .then(() => sync.reload('./static/bundle.js'))
    .catch((e) => console.error('Build error', e));
  })
}

if (!module.parent) {
  build('development').then(serve).catch((e) => console.error('Build error', e));
}

export default serve;
