## What is this?

It's a way to run untrusted code in a Docker container. The code is executed, the output is returned and the machine is discarded. It's probably not safe at all.

Currently there is only a code runner for Javascript.

## Folders

### `runner`

The runner is the docker container actually running the code. The container takes two arguments, an input and the code to execute.

### `executor`

Launches Docker containers, handles logging in and serves the frontend.

### `client`

Client side code

### `examples`

Code snippets that can be run in the runner

### `telegram`

Example integration for Telegram
